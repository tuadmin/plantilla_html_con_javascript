/**
 * @version 0.0.2
 * @param {*} elemento 
 * @param  {...any} args 
 * @returns 
 */
function tuDom(elemento,...args){
    //si elemento es string, se crea un elemento html
    if(typeof elemento === 'string'){
        elemento = document.createElement(elemento);
    }else if(typeof elemento === 'object'){
        //si elemento es un objeto, se asume que es un elemento html                
    }
    //console.log(elemento)
    
    const proxy = new Proxy({},{
        get(target,prop){
            if(prop=='dom'){
                return elemento;
            }else if(prop=='_'){
                elemento = elemento.parentElement;
                return proxy;
            }else if(prop=='empty'){                
                return (...extraArgs)=> {
                    for (let i = elemento.childNodes.length-1; i >=0 ; i--) {
                        const element = elemento.childNodes[i];
                        elemento.removeChild(element);
                    }                    
                    return proxy;
                };
            }else if(prop==='appendChild' || prop==='append'){
                return (element,props)=> {                            
                    if(element instanceof HTMLElement){
                        elemento.appendChild(element);
                        agregarAtributosAElemento(elemento,props);
                    }                                
                    else if(element.dom){
                        elemento.appendChild(element.dom);
                        agregarAtributosAElemento(elemento,props);
                    }
                    else{
                        throw new Error('El elemento a adicionar debe ser un HTMLElement o un tuDom');
                    }
                    return proxy;
                }
            }
            return (...extraArgs)=> {
                const hijo = tuDom(prop,...extraArgs);
                elemento.appendChild(hijo.dom);
                return hijo;
            }
        }
    });
    for (let index = 0; index < args.length; index++) {
        const argumento = args[index];
        if(typeof argumento === 'string'){
            elemento.appendChild(document.createTextNode(argumento));
        }else if(typeof argumento === 'object'){
            if(argumento instanceof HTMLElement || argumento instanceof DocumentFragment || argumento instanceof Text){
                elemento.appendChild(argumento);
            }else{
                agregarAtributosAElemento(elemento,argumento);
            }            
        }else if(typeof argumento === 'function'){
            argumento(proxy);
        }else if(typeof argumento === 'array'){
            for (let index = 0; index < argumento.length; index++) {
                if(typeof argumento[index] === 'object')
                    elemento.appendChild(argumento[index]);
                else
                    elemento.appendChild(document.createTextNode(argumento[index]));
            }
        }                    

    }
    function agregarAtributosAElemento(elemento,attributes){                
        for (const key in attributes) {
            if (Object.hasOwnProperty.call(attributes, key)) {
                const value = attributes[key];
                if(key=='style'){
                    switch(typeof value){
                        case "function":
                            value(childElement.dom.style);
                        case "string":
                            elemento.style.cssText = value;
                            break;
                        case "object":
                            for (const keyStyle in value) {
                                if (Object.hasOwnProperty.call(value, keyStyle)) {
                                    const valueStyle = value[keyStyle];
                                    elemento.style[keyStyle] = valueStyle;
                                }
                            }
                            break;
                    }                                
                } 
                else if(key=='on'){
                    for (const keyEvent in value) {
                        if (Object.hasOwnProperty.call(value, keyEvent)) {
                            const valueEvent = value[keyEvent];
                            elemento.addEventListener(keyEvent, valueEvent.bind(proxy),false);
                        }
                    }
                }
                else if(key=='one'){
                    for (const keyEvent in value) {
                        if (Object.hasOwnProperty.call(value, keyEvent)) {
                            const valueEvent = value[keyEvent];
                            elemento.addEventListener(keyEvent, valueEvent.bind(proxy),{once:true});
                        }
                    }
                }
                else if(key.startsWith('on')){
                    const keyEvent = key.replace('on','').toLowerCase();
                    elemento.addEventListener(keyEvent, value.bind(proxy),false);
                }
                else if(key.startsWith('one')){
                    const keyEvent = key.replace('one','').toLowerCase();
                    elemento.addEventListener(keyEvent, value.bind(proxy),{once:true});
                }
                else if(key=='data'){
                    for (const keyData in value) {
                        if (Object.hasOwnProperty.call(value, keyData)) {
                            const valueData = value[keyData];
                            elemento.dataset[keyData] = valueData;
                        }
                    }
                }
                else
                {
                    if(typeof value == "function")
                    elemento.addEventListener(key, value,false);
                    else
                        elemento.setAttribute(key, value);  
                }
                                                                            
            }
        }
    }
    return proxy;
}
function convertirBase64(texto) {
    var encoder = new TextEncoder();
    var datos = encoder.encode(texto);
    var codificado = "";
  
    for (var i = 0; i < datos.length; i++) {
      codificado += String.fromCharCode(datos[i]);
    }
  
    return btoa(codificado);
}
function boolDataset(elemento,propiedad){
    if(!(elemento instanceof HTMLElement))
        throw new Error('El elemento debe ser un HTMLElement');
    if(elemento.dataset[propiedad] == undefined)
        return false;
    switch(elemento.dataset[propiedad].toLowerCase()){
        case '':
            return elemento.hasAttribute('data-'+propiedad);    
            break;
        case 'true':
        case '1':
        case 'on':
        case 'yes':
            return true;
    }
    return false;    
}
function cargarScripts(urls,contenedorHtml) {
    if(!contenedorHtml instanceof HTMLElement)
        throw new Error('El contenedorHtml debe ser un HTMLElement');
    var contador = 0;
    return new Promise((resolve,reject)=>{
        const timeout = setTimeout(function(){
            reject('Se ha superado el tiempo de espera');
        },1000*30);
        function cargarSiguienteScript() {
            if (contador < urls.length) {
                var url = urls[contador];
                var script = document.createElement("script");
                script.src = url;
                script.onload = function() {
                contador++;
                cargarSiguienteScript();
                };
                script.onerror = function() {
                    console.error("Error al cargar " + url);
                    contador++;
                    cargarSiguienteScript();
                }
                contenedorHtml.appendChild(script);
            } else {
                clearTimeout(timeout);
                resolve(contador);// Se ejecuta cuando todos los scripts han sido cargados
                //callback(contador); 
            }
        }
        cargarSiguienteScript();
    });
  }
/**
 * @version 0.0.2
 * @author victor
 */
const Plantilla={
    base:function(){
        
        const json={
            body:null,
            link:[],
            style:[],
            script:[],
        }
       
        document.body.querySelectorAll('[src]').forEach(el => {                
            el.setAttribute('src',el.src);
        });
        document.querySelectorAll('link').forEach(link => {
            if(link.href){
                json.link.push(link.href);
            }            
        });
        document.querySelectorAll('style').forEach(style => {
            json.style.push(style.innerHTML);
        });
        document.querySelectorAll('script').forEach(script => {
            console.log(script);
            if(boolDataset(script,'ignorar')){ return;}
            if(script.src){
                json.script.push({type:'src',src:script.src});
            }
            else{
                json.script.push({type:'code',code:script.innerHTML});
            }            
        });

        const elementos = document.body.childNodes;
        json.body = "";
        for (let index = 0; index < elementos.length; index++) {
            const element = elementos[index];
            if(element instanceof HTMLElement){
                if((element.tagName == 'SCRIPT' )){                
                    continue;
                }
                json.body += element.outerHTML;
            }else if(element instanceof Text){
                json.body += element.textContent;
            }                                    
        }
        //json.body = document.body.innerHTML;
        const stringJson = JSON.stringify(json);

        window.addEventListener('load',()=>{
            
            if(!window.opener){
                console.error('No se pudo enviar la plantilla al hijo');
            }else{
                window.opener.postMessage('plantilla:'+stringJson ,'*');
            }
            //window.close();
        })
    },
    usar:function(plantillaUrl){
        plantillaUrl =  !plantillaUrl ? 'plantilla.html' : plantillaUrl;
        llaveJson = 'plantilla_' + plantillaUrl;
        const cache = localStorage.getItem(llaveJson);

        //const loading = tuDom("div",{id:"loading-wrapper"});
        //loading.div({class:"spinner-border",role:"status"}).span({class:"sr-only"},"Cargando...");

        const loading = tuDom("div",{id:"contenedor-cargando"});
        loading.div({class:"spinner-border",role:"status"}).span({class:"sr-only"},"Cargando...");
        document.body.insertBefore(loading.dom,document.body.firstChild);
        const ID_IFRAME = 'plantillaIframe';
        const renderizar = new Promise((resolve,reject)=>{
            if(cache){
                try {
                    const json = JSON.parse(cache);
                    resolve(json);
                    return;    
                } catch (error) {
                    
                }
                
            }
            window.addEventListener('message',function(e){
                e.preventDefault();
                if(e.data.startsWith('plantilla:')){
                    e.stopPropagation();
                    const stringJson = e.data.substr(e.data.indexOf(':')+1);
                    this.localStorage.setItem(llaveJson,stringJson);
                    const json = JSON.parse(stringJson);
                    window.document.querySelector('[name="'+ID_IFRAME+'"]').remove();
                    resolve(json);
                }else{
                    reject(e.data);
                }
            });

            document.addEventListener("DOMContentLoaded",function(){
                const iframe = document.createElement('iframe');
                iframe.src = 'about:blank';
                iframe.style.position = 'fixed';
                iframe.style.top = '0px';
                iframe.style.left = '0px';
                iframe.style.width = '2px';
                iframe.style.height = '2px';                
                iframe.name = ID_IFRAME;
                document.body.appendChild(iframe);
                setTimeout(function(){
                    window.open(  plantillaUrl,ID_IFRAME);
                },200);
                
                // const a  = document.createElement('a');
                // a.href = plantillaUrl;
                // a.target = '_blank';
                // a.addEventListener('click',function(e){
                //     e.preventDefault();
                //     window.open(  plantillaUrl,'_blank');
                // });
                // a.innerText = 'Abrir en otra ventana';
                // document.body.appendChild(a);
                // // setTimeout(() => {
                // //     a.click();
                // // }, 1000);
                // document.body.addEventListener('mousemove',function(){
                //     a.click();
                // },{once:true});
                //window.open(  plantillaUrl,'_blank');
            });                        
            
        });
        return renderizar.then(json=>{
            console.log('json',json);
            const textHtml = json.body;
            const parser = new DOMParser();
            const doc = parser.parseFromString("<body>"+textHtml+"</body>", 'text/html');

            const moverElementos = [];
            document.body.childNodes.forEach(element => {
                if(element.tagName && element.tagName.toLowerCase()!='script' 
                && element.tagName.toLowerCase()!='style' && element.tagName.toLowerCase()!='link'){
                    moverElementos.push(element);
                }
            });
            
            const plantilla = document.createDocumentFragment();
            window.test = doc;
            const contenedor = doc.getElementById('contenedor');
            if(contenedor){
                while (contenedor.lastChild) {
                    contenedor.lastChild.remove();
                }
            }
            
            while (doc.body.lastChild) {
                plantilla.appendChild(doc.body.firstChild);
            }
            document.body.appendChild(plantilla);
            //contenedor.appendChild(tuDom("div","adaddad").dom);
            const h1 = document.createElement('h1');
            h1.innerHTML = "Hola mundo";
            //contenedor.appendChild(h1);
            for (let index = 0; index < moverElementos.length; index++) {
                const element = moverElementos[index];
                contenedor.appendChild(element);
            }
            
            json.link.forEach(data => {
                const link = document.createElement("link");
                link.rel = "stylesheet";
                link.href = data;
                document.head.appendChild(link);
            });
            json.style.forEach(data => {
                const style = document.createElement("style");
                style.innerHTML = data;
                document.head.appendChild(style);
            });
            const scriptSrcs = [];
            const scriptTexts = [];
            // json.script.forEach(data => {
            //     const script = document.createElement("script");
            //     if(data.type=='src'){
            //         script.src = data.src;
            //     }else{
            //         script.appendChild(document.createTextNode(data.code));
            //     }
            //     document.head.appendChild(script);
            // });
            // para que se carguen en orden y no den error de dependencia
            json.script.forEach(data => {
                
                if(data.type=='src'){
                    scriptSrcs.push(data.src);
                }else{
                    scriptTexts.push(data.code);
                }
            });
            cargarScripts(scriptSrcs,document.head)
            .then(function(){
                scriptTexts.forEach(code => {
                    const script = document.createElement("script");
                    script.appendChild(document.createTextNode(code));
                    document.head.appendChild(script);
                });
            })
            .finally(function(){
                loading.dom.remove();
                console.log(loading.dom);
            });
            
        },(error)=>{
            console.error(error);
            error?alert(error):alert('Error desconocido al cargar la plantilla');
        });
    }
};