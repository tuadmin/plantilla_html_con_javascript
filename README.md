# Plantilla Html Con Javascript

Este archivo fue creado con la idea de tener o poder usar plantillas variadas con codigo javascript variado 

## preparacion de plantilla.html(layout)
Este archivo debe tener todo lo necesario para representar la plantilla lo unico necesario es un elemento #id contenedor 
## Funcionamiento

Una **pagina cualquiera**. agrega el script plantilla.js e invoca su funcion *Plantilla.usar('plantilla.html')* con ello el contenido actual de la **pagina cualquiera** sera movida al *html* obtenido del archivo *plantilla.html*

```html(plantilla.html)
<h1>cabecera</h1>
<div id="#contenedor">Este contenido sera reemplazado por la pagina solicitante</div>
<h3>Pie</h3>
```

y nuestra pagina de ejemplo
```html(uno.html)
<script src="plantilla.js"></script>
<script>Plantilla.usar("plantilla.html")</script>
<strong>esto sera insertado en la plantilla</strong>
<p id="hace_algo"></p>

<script>
document.querySelector("#hace_algo").innerHTML='<b>Este contenido es java puro</b>';
</script>
```

el resultado para el navegador sera

```html(uno.html)
<h1>cabecera</h1>
<div id="#contenedor">
<script src="plantilla.js"></script>
<script>Plantilla.usar("plantilla.html")</script>
<strong>esto sera insertado en la plantilla</strong>
<p id="hace_algo"></p>

<script>
document.querySelector("#hace_algo").innerHTML='<b>Este contenido es java puro</b>';
</script>
</div>
<h3>Pie</h3>
```

## finalidad

El fin de este pequeño proyecto es usar lo mas actual de los navegadores para intercomunicar funciones sin depender de *fetch* debido a restricciones como ya que no se puede usar el *scheme* "file://"

```
file:///carpeta/uno.html
```

con este javascript lo que se quiere lograr es *no centrarse* en codigo repetitivo, util para el aprendisaje y mas si revisan el codigo interno del mismo.

## problemas previos

antes de realizar esta version de git, se trato de usar *localStorage* y un algoritmo mas complejo pero que era mas rapido ya que se lograba en cierta forma una especie de cache. pero usar el *scheme* "file://" daba error en navegadores Firefox ya que este dividia en contextos diferentes cada archivo .html